package com.kantida.app1_62160239

import android.os.Bundle
import android.provider.AlarmClock
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class HelloActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hello_activity)

        val message = intent.getStringExtra(AlarmClock.EXTRA_MESSAGE)
        val textView = findViewById<TextView>(R.id.textViewName).apply {
            text = message
        }
    }
}
